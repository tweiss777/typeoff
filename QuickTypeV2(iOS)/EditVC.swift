//
//  EditVC.swift
//  QuickTypeV2(iOS)
//
//  Created by Tal Weiss on 12/12/16.
//  Copyright © 2016 Tal Weiss. All rights reserved.
//


//View controller for the edit profile page 
import UIKit


struct ScreenSize //detects screen size of device
{
    //gets the screen size based on the device currently running the app
    static let SCREEN_WIDTH = UIScreen.main.bounds.size.width
    static let SCREEN_HEIGHT = UIScreen.main.bounds.size.height
    static let SCREEN_MAX_LENGTH = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}




struct DeviceType // detects the device type
{
    static let IS_IPHONE_4_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
    static let IS_IPHONE_5 = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
    static let IS_IPHONE_6 = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
    static let IS_IPHONE_6P = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
}






class EditVC: UIViewController {
    //instance of main view controller where the name would be passed to     
    //Global vars go here
    var userName: String? //should be set to an optional
    var userEmoji: Character = " "
    var playerObject: PlayerObject?
    var rootVC = ViewController()
    //delegate for passing name and emoji to the root ViewController
    
    //End of global vars section
    
    
    //IBOutlets go here
    @IBOutlet weak var editName: UIButton!
    @IBOutlet weak var lvlLbl: UILabel!
    @IBOutlet weak var editProPicBttn: UIButton!
    //end of IBOutlet sections
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //print("Username is \(userName!)") // getting nil while unwrapping optional value (possibly fixed)
        
        //Set borderwidth, border color and corner radius
        editProPicBttn.layer.cornerRadius = 16
        editProPicBttn.layer.borderWidth = 3
        editProPicBttn.layer.borderColor = UIColor.white.cgColor
        
        editName.setTitle(playerObject!.getUserName(), for: .normal)
        
        
        print("UserName from player object is: \(playerObject!.getUserName())")
        lvlLbl.text! = "Level: " + String(playerObject!.getLevel())
        
        
        
        
        //If statement that will detect bigger screens
        if DeviceType.IS_IPHONE_6P{//This will detect a 7+ as well
            //increase font size
            print("you are using a device that is either a 6+ or 7+")
        }
        //if statement that will detect iphone4 or 4s
        if DeviceType.IS_IPHONE_4_OR_LESS{
            print("You are using either an iphone 4 or 4s")
            //shrink font size
        }
        
        // Do any additional setup after loading the view.
        
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    
    //IBAction functions go here
    //edit name function  
    @IBAction func editName(_ sender: UIButton) {//edit the user's name here
        let alert = UIAlertController(title: "User Name", message: "Enter your username", preferredStyle: .alert)
        alert.addTextField(configurationHandler: { (textField) in
        })
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] (_) in
            let textField = alert?.textFields![0] // Force unwrapping because we know it exists.
            
            if((textField?.text?.isEmpty)! || (textField?.text?.trimmingCharacters(in: CharacterSet.whitespaces).isEmpty)!){
                print("textfield is empty")
                alert?.message = "No name given. You must enter a user name"
                textField?.text = "" //this will rid the textfield of all trailing whitespaces
                self.present(alert!, animated: true, completion: nil)
                
            }
                
            if((textField?.text?.characters.count)! < 4 || (textField?.text?.characters.count)! > 15 ){
                print("name has less than 4 characters or exceeds 15 characters")
                alert?.message = "Username must be greater than 3 characters and no more than 15 characters long."
                textField?.text = ""
                self.present(alert!, animated: true, completion: nil)
            }
            
            else{
                //Fix issue of passing name from editvc to the vc of the main menu
                print("Setting name")
                self.userName = (textField?.text!)!
                self.playerObject?.setUsername(NewUser: self.userName!)
                self.editName.setTitle(self.userName, for: .normal)
                
            }
            
            //passed from the object 
            print("username that has been stored to our object = \(self.playerObject!.getUserName())")
            
            
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { [weak alert] (_) in
        }))
        
        
        self.present(alert, animated: true, completion: nil)
        
        
        
    }
    //edit emoji function
    @IBAction func editEmoji(_ sender: UIButton) {//edit the user's emoji here
        let alert = UIAlertController(title: "User Emoji", message: "Enter an emoji", preferredStyle: .alert)
        alert.addTextField(configurationHandler: { (textField) in
        })
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] (_) in
            let textField = alert?.textFields![0] // Force unwrapping because we know it exists.
            
            if((textField?.text?.isEmpty)! || (textField?.text?.trimmingCharacters(in: CharacterSet.whitespaces).isEmpty)!){
                print("textfield is empty")
                alert?.message = "No emoji given. You must enter an emoji."
                textField?.text = "" //this will rid the textfield of all trailing whitespaces
                self.present(alert!, animated: true, completion: nil)
                
            }
                
            
                
            else if((textField?.text?.characters.count)! > 1){
                print("text field has more than 1 charachter")
                alert?.message = "Please enter only 1 emoji"
                textField?.text! = ""
                self.present(alert!, animated: true, completion: nil)
                
            }
                
            else{
                self.userEmoji = Character((textField?.text!)!)
                self.playerObject?.setEmoji(emoji: String(self.userEmoji))
                
                
            }
            
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { [weak alert] (_) in
        }))
        
        
        self.present(alert, animated: true, completion: nil)

    
    }
    
    
    
    //End of IBAction functions 
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
