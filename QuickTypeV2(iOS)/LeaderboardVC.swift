//
//  LeaderboardVC2TableViewCell.swift
//  QuickTypeV2(iOS)
//
//  Created by Tal Weiss on 11/23/16.
//  Copyright © 2016 Tal Weiss. All rights reserved.
//

import UIKit

//struct that contains all the players information
/*since players names and stats change overtime we will use vars
 not constants*/
struct playerStats{
    var playerID: Int//player's id
    var playerName = String()//player's name
    var level: Int //players xp
    var wins: Int // number of wins
    var wpm: Int //Words per minute
    var emoji = String()
    
    
}
class LeaderboardVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    //global variables go here
    var playerObject: PlayerObject? //everything from playerObject will be stored in playerStats struct
    var leaderBoard = [PlayerObject]() //this will replace the leaderboard of type PlayerStats
    
    
    override func viewDidLoad() {
        print("You are now in the leaderboard VC")
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        //display the playerObject information for debugging purposes
        
        playerObject?.getObjectInfo()
        addToleaderBoard()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//    override func awakeFromNib() {
//        super.awakeFromNib()
//        // Initialization code
//    }
//
//    override func setSelected(_ selected: Bool, animated: Bool) {
//        super.setSelected(selected, animated: animated)
//
//        // Configure the view for the selected state
//    }
    
    let identifier = "Score Identifier"//identifier for
    
    //consider removing soon replace it with an array of type playerObject
    let leaderboard: [playerStats] = [/*playerStats(playerID: 901202,playerName: "C-Poppy",level: 50000,wins: 300,wpm: 300,emoji:"🦁"),
                                      playerStats(playerID: 901202,playerName: "I-Poppy",level: 100,wins: 32,wpm: 200,emoji:"😮"),
                                      playerStats(playerID: 901202,playerName: "AAAAAAAAAAAA",level: 90,wins: 30,wpm: 100,emoji:"🎸"),
                                      playerStats(playerID: 901202,playerName: "Jorpia42",level: 30,wins: 25,wpm: 39,emoji:"🦁"),
                                      playerStats(playerID: 901202,playerName: "Amark16",level: 25,wins: 20,wpm: 35,emoji:"😎"),
                                      playerStats(playerID: 901202,playerName: "DGeorge",level: 18,wins: 19,wpm: 31,emoji:"🤑"),
                                      playerStats(playerID: 901202,playerName: "Tweiss",level: 12,wins: 18,wpm: 26,emoji:"🇮🇱"),
                                      playerStats(playerID: 901202,playerName: "Koseii",level: 11,wins: 18,wpm: 26,emoji:"🤓"),
                                      playerStats(playerID: 901202,playerName: "BilyBlona",level: 10,wins: 16,wpm: 24,emoji:"🔥")*/
                                      
                                      ]
    
    //ONCE THE TABLE VIEW IS SET UNCOMMENT AND EDIT ACCORDINGLY
    //Functions for the table view layout
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //returns the number of cells in tableview
        return self.leaderBoard.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //deques reusable cell with the proper identifier and stores it in cell
        let cell: PlayerTableViewCell =  tableView.dequeueReusableCell(withIdentifier: self.identifier)! as! PlayerTableViewCell
        
//        if indexPath.row % 2 == 0{
//            //give the cell blue background
//            cell.backgroundColor = UIColor.init(red: 72/255, green: 143/255, blue: 204/255, alpha: 1)
//        }
//        
//        else{
//            //give the cell a grey background
//            cell.backgroundColor = UIColor(red: 144/255, green: 164/255, blue: 174/255, alpha: 0.34)
//            
//        }
        
        cell.playerName.text = "Player"
        cell.wins.text = "Wins"
        cell.level.text = "Level"
        cell.wpm.text = "WPM"
        
        cell.playerName.textColor = UIColor.red//UIColor(red: 249/255, green: 37/255, blue: 78/255, alpha: 0.34)
        
        
       
        let player = leaderBoard[indexPath.row]
        cell.playerName.text = "\(playerObject!.getUserName())"//"\(player.emoji)\(player.playerName)"
        cell.wins.text = String("\(playerObject!.getWins())")//String(player.wins)
        cell.level.text = String("\(playerObject!.getLevel())")//String(player.level)
        cell.wpm.text = String("\(playerObject!.getAverageWPM())")//String(player.wpm)
        
        
        //store struct attributes into
        return cell
    }
    
    
    //OTHER IMPORTANT FUNCTIONS
    func addToleaderBoard(){
        /* PsuedoCode for
         1)create a leaderboard array of type PlayerObject that will hold an arbitrary size
         2) if name being passed is a new username
         - append to the leaderboard list
         3) if the name is the same username
         search for the username in the list & update stats
         4) create the tableview
         */leaderBoard.append(playerObject!)
        
        
    }
    
    //Save for later
    func sortLeaderBoard(){
        //Insert code here
        //Discuss sorting with Denson before writing
        
    }
    
    
    //save this function for later 
    //exhaustive algorithm, optimize later
    func playerExists(name: String) -> Bool{
        //if true player exists
        //otherwise player does not exist
        var playerFound = false
        for player in leaderBoard{
            if player.getUserName() == name{
                print("\(name) exists")
                playerFound = true
            }
            
            else{
                print("\(name) does not exist")
                playerFound = false
            }
        }
        return playerFound
    }


}
