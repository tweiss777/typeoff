//
//  PlayerObject.swift
//  QuickTypeV2(iOS)
//
//  Created by Tal Weiss on 1/10/17.
//  Copyright © 2017 Tal Weiss. All rights reserved.
//

import Foundation
import UIKit

public class PlayerObject{
    //add a var for a UIImage here soon
    private var Username = "Enspyre"
    private var Emoji = "🦁"
    private var Level = 1
    private var exp = 0
    private var Wins = 0
    private var AverageWPM = 0.0
    private var soundOn: Bool = true
    private var notificationsOn: Bool = true
    private var vibrationOn: Bool = true
    private var profilePic: UIImage = UIImage(named: "profile-face")!//default image when object is declared
    var playerArray = [PlayerObject]() // Consider Removing

    init(Username: String, Emoji: String, Level: Int, exp: Int, Wins: Int, AverageWPM: Double, soundOn: Bool, notificationsOn: Bool, vibrationOn: Bool) {
        self.Username = Username
        self.Emoji = Emoji
        self.Level = Level
        self.exp = exp
        self.Wins = Wins
        self.AverageWPM = AverageWPM
        self.soundOn = soundOn
        self.notificationsOn = notificationsOn
        self.vibrationOn = vibrationOn
    }
    
    init(){
        
    }
    
    func addPlayer(player:PlayerObject){
        playerArray.append(player)
    }
    
    func deletePrevPlayer() -> PlayerObject {
        if !playerArray.isEmpty{
            return playerArray.removeLast()
        }
        
        else{
            print("No players are in the array")
            return PlayerObject(Username: "No name", Emoji: "🦁" , Level: -1, exp: -1, Wins: -1, AverageWPM: -1.0, soundOn:true, notificationsOn:true , vibrationOn:true)
        }
        
    }
    
    func showPlayerArray(){
        print(playerArray)
    }

    
    func setUsername(NewUser: String){
        //var result = false
        if(NewUser.count <= 15){
            if(NewUser.count < 4){

                print("name is too small")

            }

            else{
                print("size of user name does not exceed 15 characters and meets minimum")
                //result = true
//                if !playerArray.isEmpty{
//                    playerArray[playerArray.count-1].Username = NewUser
//                }
//                else{
                    Username = NewUser
//
//                }
            }
        }

        else{
            print("Size of username exceeds 15 characters")

        }


        //return result
    }

    func getUserName() -> String{
                return Username
//        }

    }


    //sets the emoji
    func setEmoji(emoji: String) /*-> Bool*/{
        //insert your code here
        if(emoji.count == 1){
            print("Emoji contains 1 character and is accepcted")
            Emoji = emoji
        }

        else{
            print("You either didn't enter an emoji or you entered more than one character")
        }

    }


    func getEmoji() -> String{
        return Emoji
    }


    //settings function

    func setVibration(toggle: Bool){
        if toggle == true{
            print("Vibration on")
            //simulate a vibration
            vibrationOn = toggle
        }
        else if toggle == false{
            print("Vibration off")
            vibrationOn = toggle
        }
    }

    func setNotifications(toggle:Bool){
        if toggle == true{
            print("Notifications on")
            notificationsOn = toggle

        }
        else if toggle == false{
            print("Notifications off")
            notificationsOn = toggle
        }

    }

    func setSound(toggle:Bool){
        if toggle == true{
            soundOn = toggle
            print("Sound on")



        }
        else if toggle == false{
            print("Sound off")
            soundOn = toggle
        }
    }

    func getVibrationSetting() -> Bool{
        return vibrationOn
    }

    func getSoundSetting() -> Bool {
        return soundOn
    }

    func getNotificationSettings() -> Bool{
        return notificationsOn
    }

    func temporaryExp(didWin:Bool) -> Bool{//temporary func to increase exp, This is going to be re-written
        var didLevelUp = false
        if(didWin == true){
            Wins += 1
            exp += 1
            if(exp == 2){
                Level += 1
                exp = 0
                didLevelUp = true
            }
        }
        return didLevelUp
    }
    
    func getLevel() -> Int {
        return Level
    }
    
    
    
    func getWins() -> Int{
        return Wins
    }

    //function that will calculate average WPM
    //consider re-writing this function by using number of wins to calculate averagewpm
    func setAverageWPM(totalwpm:Double){
        AverageWPM = totalwpm
    }
    
    func getAverageWPM() -> Double {
        return AverageWPM
    }

    func getObjectInfo(){//function that will display all the player's information and settings
        print("==========OBJECT INFORMATION==========")
        print("Player name is: \(Username)")
        print("Player emoji is: \(Emoji)")
        print("player level: \(Level)")
        print("Player XP: \(exp)")
        print("Wins: \(Wins)")
        print("Average WPM \(AverageWPM)")
        print("===Game Settings===")
        print("Vibration on: \(vibrationOn)")
        print("Sound on: \(soundOn)")
        print("Notifications on: \(notificationsOn)")
    }
    
    
    //untested getter and setter methods for the profile pic
    func setPic(image: String){
        profilePic = UIImage(named: image)!
    }

    func getPic() -> UIImage {
        return profilePic
    }

}
