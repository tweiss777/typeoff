//
//  PlayerTableViewCell.swift
//  QuickTypeV2(iOS)
//
//  Created by Tal Weiss on 11/23/16.
//  Copyright © 2016 Tal Weiss. All rights reserved.
//

import UIKit
//this is a custom tableviewcell class
class PlayerTableViewCell: UITableViewCell {
    @IBOutlet weak var playerName: UILabel!
    @IBOutlet weak var wins: UILabel!
    @IBOutlet weak var level: UILabel!
    @IBOutlet weak var wpm: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
