//
//  PracticeViewController.swift
//  QuickTypeV2(iOS)
//
//  Created by Tal Weiss on 11/10/16.
//  Copyright © 2016 Tal Weiss. All rights reserved.
//

import UIKit
import AVFoundation
import AudioToolbox

//IMPORT THE CODE FROM THE GAMEPLAY PROJECT

//create the stack struct here!

//Remove the quitbutton since it is built in to the pause menu code below

//FOR FUTURE USAGE
//struct promptStack<Element>{
//    var prompts = [Element]()
//    mutating func push(_ prompt: Element){
//        prompts.append(prompt)
//        
//    }
//    
//    mutating func pop() -> Element?{
//        if !prompts.isEmpty{
//            print("returning the top of the stack")
//            return prompts.removeLast()
//        }
//        else{
//            print("returning nil")
//            return nil
//        }
//    }
//    //consider revising
//    mutating func stackIsEmpty() -> Bool {
//        if !prompts.isEmpty{
//            return false
//        }
//        else{
//            return true
//        }
//    }
//    
//}

//end of stack struct
class PracticeViewController: UIViewController {
    @IBOutlet weak var finalWPM: UILabel!//WPM recorded
//    @IBOutlet weak var userEmoji: UILabel!//User's emoji
    @IBOutlet weak var currentTime: UILabel!//Current game time
    @IBOutlet weak var completeLbl: UILabel!//prompt that the user will see
    @IBOutlet weak var gameBG: UIImageView!//Reference to the game background
    @IBOutlet weak var promptBG: UIImageView!
    @IBOutlet weak var lblPrompt: UILabel!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var pauseButton: UIButton!
    @IBOutlet weak var backGroundVC: UIView!
    @IBOutlet weak var categoryLbl: UILabel!
    
    
    
    //In-game variables and booleans
    var delegate: playerObjectDelegate? //passes data
    
    var audioPlayer: AVAudioPlayer? //for sound effects

    var timer = Timer() //initializes timer object
    var countUpSeconds = 0 //typing label Seconds
    var countUpMiliseconds = 0 //typing label Miliseconds
    var countDown = 3 //countdown to game start will be measured in seconds
    
    var placeHolder: Int = 0 //placeholder for the game
    var incorrect = false //boolean for incorrect letter
    var winner = false //bool to determine game outcome
    var didCheat = false //bool that determines if the user cheats
    var prompt = 0 //the current prompt number
    var charCount = 1 //records number of correct characters
    
    var isPaused = false // bool to indicate if the game is paused or not
    //var storePaused = "" //stores what was typed when pause button was pressed
    var gameDidstart = false //bool indicate if the game started
    var resetFrompause = false
    var didLevelUp = false
    //var usedPrompts = promptStack<String>() //FOR FUTURE USE
    var usedPrompts = [String]()
    var totalWPM = [Double]()
    var playerObject: PlayerObject?
    
    //End of in-game varibales sections
    
    var promptList = ["All I want for christmas is you","You would even say it glows","A partridge in a pear tree","Three french hens two turtle doves","Five golden rings four calling birds","Walking in a winter wonderland","Oh the weather outside is frightful","Now the jingle hop has begun","We wish you a merry christmas","Rudolph the red nosed reindeer","Santa claus is coming to town","Jingle bells jingle all the way","For a sleigh ride together with you","Frosty the snowman jolly soul","Let it snow let it snow let it snow","Worry less and smile more","Do what you like the best","Be the best version of you","Everything will be alright","Beginnings are always messy","Everyday is a second chance","You get what you settle for","Never let go of your dreams","Lies hurt more than the truth","It is not as bad as you think","Be happy be bright and be you","Make your dreams your reality","Do not keep your dreams secret","You are stronger than you think","Life is a journey and not a race","All limitations are self imposed","Nothing great ever came that easy","Hope is the heartbeat of the soul","Mistakes are proof you are trying","Every moment is a fresh beginning","Our thoughts determine our reality","Use your brain and follow your heart","One decision can change your life","Always choose to be optimistic","Love yourself for who you are","Speak your mind even if no one hears","Be humble because you could be wrong","Be informed instead of opinionated","On a mission to have a good time","It is not always about being the best","We aint ever getting older","Got me walkin side to side","Twenty four karat magic in the air","Never let you go never let me down","The way I used to love you","Please dont make any sudden moves","No stars to your beautiful","What can I say its complicated","And I hope you know I wont let go","I know I can treat you better","Come on come on turn the radio on","Do it all in the name of love","Lightning strikes everytime she moves","I pray to make it back in one piece","This queen dont need a king","Am I the only living soul around","I got this feeling inside my bones","Our love aint water under the bridge","Once I was seven years old","Im not just one of your many toys","You used to call me on my cell phone","All we need is somebody to lean on","I must have called a thousand times","Is it too late to say im sorry now","And baby now we got bad blood","And I didnt wanna write a song","No one can be just like me any way","House so empty need a centerpiece","Dont believe me just watch","Theres you in everything I do","Life is like a box of chocolates","Were not in kansas anymore","They call it a royal with cheese","After all tomorrow is another day","If you build it he will come","Where were going we dont need roads","Say hello to my little friend","I am serious and dont call me shirley","Houston we have a problem","You cant handle the truth","You play ball like a girl","Ill have what shes having","You do not talk about fight club","Theres no place like home","May the force be with you","Youre gonna need a bigger boat","Theres no crying in baseball","I feel the need the need for speed","Get busy living or get busy dying","But theyll never take our freedom","Layla youve got me on my knees","And the world will be as one","Found out what it means to me","Here we are now entertain us","There will be an answer let it be","I get so lonely I could die","That moon is the only light you see","But now since I see you in his arms","I wanna rock and roll all night","And this bird you cannot change","Curry thompson durant and green","Kobe went out with a sixty point game","Just a kid from northeast ohio","Tim duncan was a nineteen year spur","Ballin on em like im james harden","Warriors blew a three one lead","Russell has ice in his veins","The greek freak rules milwaukee","Westbrook is the best since oscar","The king has made six straight finals","The seahawks have the legion of boom","Tom brady cannot beat Eli","Will the jets ever win it all again","Peyton went out with a ring","A football team may soon be in vegas","Dak and zeke are the future","Kirk wants to know if you like that","Ben bell and brown for the steelers","Will the browns win a game this year","All about that action boss","The cubs have broken the curse","The indians are on the rise","Trout or harper you decide","Jeter will always be the captain","Oakland loves playing moneyball","One two three strikes youre out","Single double triple home run","Twenty seven outs in a game","Take me out to the ballgame","Baseball is our national pastime","Walt jesse skyler hank flynn","Peter lois chris meg stewie brian","Chandler monaca ross rachel joey","Sheldon leonard howard raj penny","Michael jim pam dwight andy","Ted marshall lily robin barney","Eric donna hyde kelso jackie fez","Rick carl daryl michone carol maggie","Homer marge bart lisa maggie","Francis claire doug zoe lucas"
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //the highlighting part of the code is not working
//        highlight the current place holder here
//        var texthighlight = NSMutableAttributedString(string: lblPrompt.text!)
//        texthighlight.addAttribute(NSBackgroundColorAttributeName, value: UIColor.green, range: NSRange(location: 0, length: placeHolder+1))

        
        //Call player object info here 
        playerObject?.getObjectInfo()
        
        //sets player emoji
        //userEmoji.text! = playerObject!.getEmoji()
        
        
        //this hides the backbutton since we have a quit button in place
        self.navigationItem.setHidesBackButton(true, animated:true) //FOR SOME REASON THE BACK BUTTON IS STILL SHOWING
        
        // Do any additional setup after loading the view.
        textField.isHidden = true
        lblPrompt.lineBreakMode = NSLineBreakMode.byWordWrapping
        lblPrompt.numberOfLines = 3
        
        //first random prompt is chosen
        //move these three lines to startgame function
        prompt =  Int(arc4random_uniform(UInt32(promptList.count-1)))
        lblPrompt.text! = "" //promptList[prompt] //We start with a random prompt in the list
        //usedPrompts.append(promptList.remove(at: prompt))
        
        //used for debugging purposes
        print("prompts left in the promptlist: \(promptList.count)")
        print("prompt #: \(prompt)")
        




        
        
        //
        
        textField.autocapitalizationType = .none//Auto capitlizes the first word in the textfield
        //textField.isUserInteractionEnabled = false
        hideStats()
        hideGame()
        countDownTimer()
        countUpMiliseconds = 0
        countUpSeconds = 0
        
//        let stringWithAttributes = NSMutableAttributedString(string: lblPrompt.text!)
//        stringWithAttributes.addAttribute(NSBackgroundColorAttributeName, value: UIColor.init(red: 255/255, green: 255/255, blue: 0/255, alpha: 0.80), range: NSRange(location: 0, length: 1 ))
//        
        //corner radius and border color
        backGroundVC.layer.borderWidth = 2
        backGroundVC.layer.borderColor = UIColor(red: 255/255, green: 23/255, blue: 68/255, alpha: 1.0).cgColor
        backGroundVC.layer.cornerRadius = 15
        
        //hide navigationbar
        self.navigationController?.isNavigationBarHidden = true
        
        //corner radius to category label
        categoryLbl.layer.cornerRadius = 5
        
        //corner radius to the promptlbl
        lblPrompt.layer.cornerRadius = 10
        
        textField.becomeFirstResponder()

        
    }
    
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }

    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    //IBACTIONS GO HERE
    @IBAction func pauseButton(_ sender: Any) {
        showPausemenu()//see the body of this function for more details
    }
    
    
    
    @IBAction func textFieldChange(_ sender: UITextField) {
        //following print statements are used for debugging purposes DO NOT DELETE!!
        print("Curent charcount = \(charCount)")
        print("textfield count = \(textField.text!.count)")
        print("current placeholder: \(placeHolder)")
        
        
        
        if(lblPrompt.text!.isEmpty){
            print("Game has not started yet...")
            textField.text! = ""
        }
        // Period is recognized as space!!!
        if(!lblPrompt.text!.isEmpty){
            if(!winner){//If not a winner
                            //check for cheating'
                if(charCount < textField.text!.count){ //checks if string length greater than charCount
                    //if(textField.text!.characters.count > lblPrompt.text!.characters.count ){//Detects if the user cheated or not
                    didCheat = true
                    print("didCheat: \(didCheat)")
                    
                    
                    
                    //END OF NESTED IF STATEMENT
                }
                
                //comparing textfield input to prompt
                let txtField = textField.text!
                let lbl = lblPrompt.text!
                var tField1 = "-1"
                var lbl1 = "-0"
                
                
                
                if(placeHolder < textField.text!.count){
                    tField1 = String(txtField[txtField.index(txtField.startIndex, offsetBy: placeHolder)])
                }
                if(placeHolder < lblPrompt.text!.count){
                    lbl1 = String(lbl[lbl.index(lbl.startIndex, offsetBy: placeHolder)])
                    
                    //highlight current placeholder
                }
                //lblPrompt.text! = textField.text!
                print("comparing \(tField1) with \(lbl1)")
                
                if placeHolder == 0{
                    //force capitalize the first letter
                    tField1 = tField1.uppercased()
                    
                }
                
                //If the letter entered matches the letter of the placeholder index
                if(tField1 == lbl1){
                    incorrect = false
                    
                    //Highlights the matched letter green to indicate that it is correct
                    let stringWithAttributes = NSMutableAttributedString(string: lblPrompt.text!)
                    stringWithAttributes.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.green, range: NSRange(location: 0, length: placeHolder+1 ))//placeholder gets incremented within stringWithAttributes
                    if(placeHolder + 1 < lblPrompt.text!.count){
                        stringWithAttributes.addAttribute(NSAttributedStringKey.backgroundColor, value: UIColor.init(red: 255/255, green: 255/255, blue: 0/255, alpha: 0.80), range: NSRange(location: placeHolder + 1, length: 1 ))
                    }
                    

                    
                   // stringWithAttributes.addAttribute(NSBackgroundColorAttributeName, value: UIColor.white, range: NSRange(location: 0, length: placeHolder+1))

                    
                    
                    
                    lblPrompt.attributedText = stringWithAttributes
                    placeHolder+=1//placeHolder also gets incremented here
                    print("placeHolder incremented to \(placeHolder)")
                    
                    //increment charcount by 1 if the game has already started
                    charCount += 1
                    
                    
                    //print statement used for debugging purposes
                    print("num of characters in textfield = \(textField.text!.count)")
                    
                    if(playerObject?.getSoundSetting() == true){ //untested
                        print("playing sound")
                        playSound(name: "hitsound", format: "wav")
                    }
                    
                }
                    
                    
                else{//If the letter is incorrect
                    print(placeHolder)//output placeholder in the console
                    incorrect = true //incorrect boolean will be true
                    if(tField1 == "-1"){
                        //print statement used for debugging purposes
                        print(textField.text!.count)
                        print("Above is char count")
                        let index = lblPrompt.text!.index(lblPrompt.text!.startIndex, offsetBy: placeHolder)//INDEX
//                        textField.text = lblPrompt.text?.substring(to: index)
                        textField.text = String((lblPrompt.text?[..<index])!)
                        
                    }
                        
                    else if(incorrect || placeHolder == 0){
//                        textField.text = textField.text?.substring(to: textField.text!.index(before: textField.text!.endIndex))
                        textField.text = String((textField.text?[..<textField.text!.index(before: textField.text!.endIndex)])!)
                    }
                    
                    
                    let stringWithAttributes = NSMutableAttributedString(string: lblPrompt.text!)
                    stringWithAttributes.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.green, range: NSRange(location: 0, length: placeHolder ))
                    
                    
                    stringWithAttributes.addAttribute(NSAttributedStringKey.backgroundColor, value: UIColor.init(red: 229/255, green: 57/255, blue: 53/255, alpha: 0.80), range: NSRange(location: placeHolder, length: 1 ))
                    
                    
                    if playerObject?.getVibrationSetting() == true{
                        print("vibration triggered")
                        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
                    }
                    
                    
                    
                    lblPrompt.attributedText = stringWithAttributes
                    
                    
                }
                //storePaused = textField.text!
            }
            if(didCheat){
                pauseButton.isEnabled = false
                lblPrompt.text = "Cheating of any kind will not be tolerated!!!!"
                textField.isUserInteractionEnabled = false
                print("textfield:")
                print(textField.text!)
                print("Curent charcount = \(charCount)")
                print("textfield count = \(textField.text!.count)")
                print("current placeholder: \(placeHolder)")

                DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(3), execute: {
                    // Put your code which should be executed with a delay here
                    self.resetGame()
                })
            }
        
            if(placeHolder == lblPrompt.text!.count){
                //if you win
                pauseButton.isEnabled = false
                winner = true
                timer.invalidate()
                lblPrompt.text = "You Win!👏🏆"
                //insert wpm function and seconds to minutes function here
                let finaltime = Double(String(countUpSeconds) + "." + String(countUpMiliseconds))
                let minutes = convertToMinutes(seconds: finaltime!)
                totalWPM.append(round(calcWPM(c: Double(charCount), t: minutes)))
                didLevelUp = (playerObject?.temporaryExp(didWin: winner))!//increments win by 1
                print(totalWPM)
                finalWPM.text! = String(round(calcWPM(c: Double(charCount), t: minutes))) + " WPM"
                textField.isUserInteractionEnabled = false
                let alertController = UIAlertController(title: "Level Up!", message: "Congratulations, you reached level \(playerObject!.getLevel()) 🏆👏🎉", preferredStyle: .alert)
                if(winner){
                    showStats()
                    var secondsToDismiss = 3
    //                if didLevelUp == true{
    //                    secondsToDismiss = 6
    //                    DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(3), execute: {
    //                        if(self.playerObject?.getSoundSetting() == true){
    //                            self.playSound(name: "COD level up", format: "mp3")
    //
    //                        }
    //                        self.present(alertController, animated: true, completion: nil)
    //                        self.didLevelUp = false
    //                        
    //                    })
    //
    //                }
                    DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(secondsToDismiss), execute: {
                        // Put your code which should be executed with a delay here
                        alertController.dismiss(animated:true, completion: nil)
                        self.resetGame()
                    })
                }
            }
        }
    }
    //END OF IBACTION SECTION
    
    
    
    //OTHER IMPORTANT FUNCTIONS
    func showPausemenu(){
        print("pauseGame() has started")
            pauseGame()
        
        //pause menu initialized
        let pauseMenu = UIAlertController(title: "Game Paused", message: "Choose an option", preferredStyle: UIAlertControllerStyle.alert)
        //pause functions called
        let resume =  UIAlertAction(title: "Resume", style: UIAlertActionStyle.default) {
            UIAlertAction in
            //create a resumeGame() function, and call it here
            self.resumeGame()
            
        }
        
        let quit = UIAlertAction(title: "Quit", style: UIAlertActionStyle.default) {
            UIAlertAction in
            //self.playerObject?.setAverageWPM(totalwpm: self.totalWPM)
            //have this take you back to the main menu
            self.calculateWPM()
            self.playerObject?.getObjectInfo()
            //self.playerObject?.addPlayer(player: self.playerObject!)
            self.playerObject?.showPlayerArray()
            self.delegate?.setLevel(level: self.playerObject!.getLevel())
            self.navigationController?.popToRootViewController(animated: true)
        }
        
        let restart = UIAlertAction(title: "Restart", style: UIAlertActionStyle.default) {
            UIAlertAction in
            self.resetFrompause = true
            self.resetGame()
        }

        
        //pause buttons added
        pauseMenu.addAction(resume)
        pauseMenu.addAction(restart)
        pauseMenu.addAction(quit)
        
        //pause menu called
        self.present(pauseMenu, animated: true, completion: nil)
        
        

    }
    
    //Function to calculate WPM
    func calcWPM(c: Double,t: Double) -> Double{
        //assumes the time given is in minutes
        //test functions that will return the number of words per minute
        //c is all typed entries
        //t is the time
        
        let wpm = (c/5)/t
        
        return wpm
    }
    
    func convertToMinutes(seconds: Double) -> Double{
        let minutes = seconds/60
        return minutes
    }

    func calculateWPM(){
        var calcAverageWPM: Double = 0
        if !totalWPM.isEmpty{
            print("Calculating average WPM")
            for wpm in totalWPM{
                calcAverageWPM = calcAverageWPM + wpm
            }
        }
        let final_wpm = round(calcAverageWPM/Double(totalWPM.count))
        print("Average WPM is \(final_wpm)")
        playerObject?.setAverageWPM(totalwpm: final_wpm)
        
    }
    
    //timerupdate function
    func StopWatchTimer(){
        timer = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(PracticeViewController.TimerUpdateTimeLabel), userInfo: nil, repeats: true)
    }
    
    //countdown until the game starts
    func countDownTimer(){
        //this will be called in our reset game function
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(PracticeViewController.TimeUpdateCountdown), userInfo: nil, repeats: true)
    }
    
    
    //function that updates the countdown timer
    @objc func TimeUpdateCountdown(){
        if didCheat == true{
            countDown = 4
            didCheat = false
        }
        
        textField.becomeFirstResponder()

            //this is where the 3..2..1...Go images will be
        
        
        if countDown == 3{
            gameBG.image = UIImage(named: "ready")
            
        }
        
        if countDown == 2{
            gameBG.image = UIImage(named: "set")
            
        }
        
        if countDown == 1{
            gameBG.image = UIImage(named: "go")
            
        }
        
        if countDown == 0{
            //start the game
            timer.invalidate()
            startGame()
        }
        print("game starts in \(countDown)")
        countDown -= 1
        
        
    }
    
    
    //function that makes the timer tick
    @objc func TimerUpdateTimeLabel(){
        countUpMiliseconds += 1//increments miliseconds by 1
        
        if(countUpMiliseconds == 100){
            countUpMiliseconds = 0
            countUpSeconds += 1
        }
        
        var seconds = String(countUpSeconds)
        var miliseconds = String(countUpMiliseconds)
        
        if (countUpMiliseconds < 10) {
            miliseconds = "0" + miliseconds
        }
        
        if (countUpSeconds < 10) {
            seconds = "0" + seconds
        }
        
        currentTime.text = seconds + "." + miliseconds + " seconds"
        
        if (didCheat) {//if cheating was caught timer gets invalidated
            timer.invalidate()
            //display a popup that will make the user go back to the previous prompt
            
        }
    
    }
    
    
    func resetGame(){
        textField.becomeFirstResponder()
        pauseButton.isEnabled = true
        gameDidstart = false
        //pauseButton.isEnabled = true
        //pauseButton.isHidden = false
        
        hideStats()
        hideGame()
        charCount = 1
        
        
        if (didCheat == true || resetFrompause == true){
            lblPrompt.text! = lblPrompt.text!
            didCheat = false
            resetFrompause = false
            textField.resignFirstResponder()
        }
        else{
            //if promptlist is empty re-insert used prompts
            if promptList.isEmpty{
                print("promptList has \(promptList.count) prompts")
                print("Re-adding prompts")
                for usedPrompt in usedPrompts{
                    promptList.append(usedPrompts.removeFirst())
                    print("Prompts in promptList \(promptList.count)")
                }
            }

            prompt = Int(arc4random_uniform(UInt32(promptList.count-1)))
            print("prompt #: \(prompt)")
            lblPrompt.text! = promptList[prompt]
            print("Prompt used = \(lblPrompt.text!)")
            usedPrompts.append(promptList.remove(at: prompt))
            print("prompts left in the promptlist: \(promptList.count)")
        }
        
        
        textField.text = ""
        //textField.isUserInteractionEnabled = false
        countUpSeconds = 0
        countUpMiliseconds = 0
        textField.autocapitalizationType = .sentences
        
        placeHolder = 0
        
        incorrect = false
        winner = false
        countDown = 3
        countDownTimer()


    }
    
    func startGame(){//call this in the countdown function
        //unhide the prompt 
        //start the timer
        //enable the textfield
        print("start game")
        gameDidstart = true
        gameBG.image = UIImage(named: "TypeOffBG2")
        //timerBG.isHidden = false
        currentTime.isHidden = false
        categoryLbl.isHidden = false
        promptBG.isHidden = false
        textField.isUserInteractionEnabled = true
//      pauseButton.isHidden = false
        //pauseButton.isEnabled = true
        lblPrompt.isHidden = false
        textField.becomeFirstResponder()
        //start timer below
        backGroundVC.isHidden = false
        lblPrompt.text! = promptList[prompt]
        usedPrompts.append(promptList.remove(at: prompt))
        charCount = 1
        StopWatchTimer()
        
        
    }
    
    func showStats(){
        //reveals the yellow box at the end of the game
        finalWPM.isHidden = false
        //finalTime.isHidden = false
        //completeLbl.isHidden = false
//        pauseButton.isEnabled = false
        //userEmoji.isHidden = false
        
        //finalTime.text! = currentTime.text!

        //function complete
        
    }
    
    func hideStats(){//call this in viewdid load and in your reset game function
        finalWPM.isHidden = true
        //finalTime.isHidden = true
        //completeLbl.isHidden = true
        //userEmoji.isHidden = true

        //function complete
        
    }
    
    func hideGame(){//call this in viewdid load and in your reset game function
        //hide the prompt
        //hide the timer
        //hide the useremoji
        //timerBG.isHidden = true
        categoryLbl.isHidden = true
        currentTime.isHidden = true
        lblPrompt.isHidden = true
        backGroundVC.isHidden = true
        promptBG.isHidden = true
    }
    
    func pauseGame(){ //for use in the showPausemenu() function
        print("Pausing game")
        timer.invalidate()
        //textField.isUserInteractionEnabled = false
        //textField.isEnabled = false
        isPaused = true
        print("isPaused: \(isPaused)")
        //            pausebutton.title! = "Resume"
        lblPrompt.isHidden = true
        //            quitButton.isHidden = false
        print("Game has been paused")

    }

    func resumeGame(){//for use in the showPausemenu() function
        //when the game is resumed
                    //textField.text! = storePaused //consider removing soon
                    print("Resuming game")
                    //textField.isUserInteractionEnabled = true
                    //textField.isEnabled = true
                    if(gameDidstart == false){
                        countDownTimer()
                        //pausebutton.title! = "Pause"
                        //textField.isUserInteractionEnabled = true
                        print("game did start")
                    }
        
                    else{
                        print("Time to disable timer")
                        StopWatchTimer()
                        //textField.isUserInteractionEnabled = true
                        lblPrompt.isHidden = false
        
        
                   }
                    isPaused = false
                    print("isPaused: \(isPaused)")
        
        //CONSIDER DELETING THE FOLLOWING COMMENTED OUT LINES OF CODE BELOW
        //            quitButton.isHidden = true
        //            self.navigationItem.setHidesBackButton(true, animated:true)
        //            print("Hiding has been done")
                    print("textfield count = \(textField.text!.count)") //show the count reverted before pausing
    }
    
    
    func playSound(name:String, format: String){//function that will play  sound
        let url = Bundle.main.url(forResource: name, withExtension: format)!
        
        do{
            audioPlayer = try AVAudioPlayer(contentsOf: url)
            guard let audioPlayer = audioPlayer else { return }
            audioPlayer.prepareToPlay()
            audioPlayer.play()
        }
        catch let error as NSError {
            print(error.description)
        }
        
    }

    //END OF SECTION
    
    
    
    //USED FOR SEGUES
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
