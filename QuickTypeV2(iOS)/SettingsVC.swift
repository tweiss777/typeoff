//
//  SettingsVC.swift
//  QuickTypeV2(iOS)
//
//  Created by Tal Weiss on 11/15/16.
//  Copyright © 2016 Tal Weiss. All rights reserved.
//
//maybe create a struct to hold all the actions of each switch
import UIKit
import AudioToolbox

protocol playerObjectDelegate {
    func setNameInMainVC(name: String)
    func setLevel(level: Int)
}

class SettingsVC: UIViewController{
    
    //global variables
    var playerObject: PlayerObject?
    var username: String = ""
    var delegate: playerObjectDelegate? // Consider removing delegate
    //end of global variables
    
    
    //IBOutlets go here
    @IBOutlet weak var toggleSound: UISwitch!
    @IBOutlet weak var toggleVibrate: UISwitch!
    @IBOutlet weak var toggleNotifications: UISwitch!
    @IBOutlet weak var backgroundVC: UIView!
    
    //end of button labels


    override func viewDidLoad() {
        super.viewDidLoad()
        
        //outputs the playerobject info to the debugger
        playerObject?.getObjectInfo()
        
        /*
         Since playerObject is initialized you will set the toggle switches based on the settings
        */
        //have the switches toggled programatically
        if playerObject?.getSoundSetting() == true{
            print("switching sound on")
            toggleSound.setOn(true, animated: true)
        }
        else{
            toggleSound.setOn(false, animated: true)
        }
        
        if playerObject?.getVibrationSetting() == true{
            print("switching vibration on")
            toggleVibrate.setOn(true, animated: true)
        }
        else{
            toggleVibrate.setOn(false, animated: true)
        }
        
        if playerObject?.getNotificationSettings() == true {
            print("switching notifications on")
            toggleNotifications.setOn(true, animated: true)
        }
        else{
            toggleNotifications.setOn(false, animated: true)
        }
        
        //set the background corner radius for the uiswitches
        toggleSound.layer.cornerRadius = 16
        toggleVibrate.layer.cornerRadius = 16
        toggleNotifications.layer.cornerRadius = 16
        
        //gives backgroundVC a corner radius
        backgroundVC.layer.cornerRadius = 15
        //removes text from the back button
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.plain, target: nil, action: nil)



        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if self.isMovingFromParentViewController{
            self.delegate?.setNameInMainVC(name: playerObject!.getUserName())
        }
    }
    
    @IBAction func toggleSound(_ sender: UISwitch) {
        if toggleSound.isOn{
            playerObject?.setSound(toggle: true)
        }
        else{
            playerObject?.setSound(toggle: false)
        }
    }

    @IBAction func toggleVibrate(_ sender: UISwitch) {
        if toggleVibrate.isOn{
            //trigger vibration
            playerObject?.setVibration(toggle: true)
            AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))

        }
        else{
            playerObject?.setVibration(toggle: false)
        }


    }

    @IBAction func toggleNotifications(_ sender: UISwitch) {
        //toggle notifications on or off
        if toggleNotifications.isOn {
            playerObject?.setNotifications(toggle: true)
        }
        else{
            playerObject?.setNotifications(toggle: false)
        }

    }

    //All other functions







     //MARK: - Navigation

//     In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "PassToProfile"{
            if let destinationVC = segue.destination as? EditVC{
                //sdestinationVC.delegate = self
                let playerData = playerObject // passes the object with player info to next vc
                destinationVC.userName = playerObject?.getUserName()
                destinationVC.playerObject = playerData

            }
        }

    }


}
