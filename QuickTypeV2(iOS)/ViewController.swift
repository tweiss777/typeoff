//
//  ViewController.swift
//  QuickTypeV2(iOS)
//
//  Created by Tal Weiss on 11/9/16.
//  Copyright © 2016 Tal Weiss. All rights reserved.
//

//



import UIKit
class ViewController: UIViewController, playerObjectDelegate {

    //IBOUTLETS
    @IBOutlet weak var lvlLbl: UILabel!
    @IBOutlet weak var cPoppyCoinsLbl: UILabel!
    @IBOutlet weak var livesLbl: UILabel!
    @IBOutlet weak var usrNameLbl: UILabel!//the user's name
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var practiceButton: UIButton!
    @IBOutlet weak var friendsButton: UIButton!
    @IBOutlet weak var randomButton: UIButton!

    //instance of the PlayerObject class
    var playerObject = PlayerObject()
    


    override func viewDidLoad() {
        super.viewDidLoad()
        
        //this will show all the settings and information of player when view loads
        playerObject.getObjectInfo()


        

        //lines 22-26 is the code necessary to make the navigation bar completely transparant
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = UIColor.clear
        //END OF CODE SEGMENT

        
        //Removes the text from the back button and changes the color
                self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        //END OF CODE SEGMENT

        /*This will give the profile pic on top right corner of the
            screen a circular border, border color.*/
        profilePic.layer.cornerRadius = profilePic.frame.size.width/2
        profilePic.clipsToBounds = true
        profilePic.layer.borderWidth = 2
        profilePic.layer.borderColor = UIColor.white.cgColor
        
        //changes button font size according to device size
        if DeviceType.IS_IPHONE_6 {
            //set button font size to 50
            practiceButton.titleLabel?.font = UIFont(name: "Titillium-Regular", size: 50)
            friendsButton.titleLabel?.font = UIFont(name: "Titillium-Regular", size: 50)
            randomButton.titleLabel?.font = UIFont(name: "Titillium-Regular", size: 50)
        }
        
        if DeviceType.IS_IPHONE_6P{
            //set button font size to 60
            practiceButton.titleLabel?.font = UIFont(name: "Titillium-Regular", size: 60)
            friendsButton.titleLabel?.font = UIFont(name: "Titillium-Regular", size: 60)
            randomButton.titleLabel?.font = UIFont(name: "Titillium-Regular", size: 60)
        }
        


        //store the value of the global variable into the label when the app first loads
        usrNameLbl.text! = playerObject.getUserName()
        lvlLbl.text! = "Level " + String(playerObject.getLevel())



        // Do any additional setup after loading the view, typically from a nib.
        //self.navigationController?.setNavigationBarHidden(true, animated: true)//disables navigation bar
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func setNameInMainVC(name: String){
        print("Setting name in the username labele")
        usrNameLbl.text! = name
    }
    func setLevel(level: Int){
        lvlLbl.text! = "Level " + String(level)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        let PlayerInfo = playerObject

        if segue.identifier == "PassToSettings"{

            if let DestViewbController: SettingsVC = segue.destination as? SettingsVC{
                    DestViewbController.delegate = self
                    DestViewbController.playerObject = PlayerInfo

            }
        
            

        }
        
        
        if let DestViewController: PracticeViewController = segue.destination as? PracticeViewController{
                DestViewController.playerObject = PlayerInfo
                DestViewController.delegate = self
            }
        
        
        if let DestViewController: LeaderboardVC = segue.destination as? LeaderboardVC{
                DestViewController.playerObject = PlayerInfo
            }
        


    }
    
//    //Consider placing this function within the playerObject model
//    func playSound(name:String, format: String){//function that will play  sound
//        let url = Bundle.main.url(forResource: name, withExtension: format)!
//        
//        do{
//            audioPlayer = try AVAudioPlayer(contentsOf: url)
//            guard let audioPlayer = audioPlayer else { return }
//            audioPlayer.prepareToPlay()
//            audioPlayer.play()
//        }
//        catch let error as NSError {
//            print(error.description)
//        }
//    
//    }



}
